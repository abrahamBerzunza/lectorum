import React from 'react';
import './styles.css';
import logo from './lectorum-sm.png';

function Navbar() {
  return(
    <nav className="amber darken-1">
      <div className="nav-wrapper container">
        <a href="/main" className="brand-logo">
          <img src={logo} alt="logo" width="200" className="nav-img"/>
        </a>
      </div>
    </nav>
  );
}

export default Navbar;
