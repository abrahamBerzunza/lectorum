import React from 'react';
import './styles.css';
import Navbar from '../Navbar';
import Metadata from '../Metadata';
import Recomendations from '../Recomendations';
import { Link } from 'react-router-dom';

function Details(props) {
  return(
    <div>
      <Navbar />
      <Metadata books={props.books}/>
      <Recomendations books={props.books}/>
    </div>

  );
}

export default Details;
