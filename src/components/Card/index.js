import React from 'react';

function Card(props) {
  return(

    <div class="book-item">
      <img class="book-image" src={props.image}/>
      <div class="overlay">
        <div class="content">
          <p><strong class="book-title">{props.name}</strong></p>
          <a href={`/details?id=${props.id}`} class="pink accent-2 waves-effect waves-light btn">
            Más
          </a>
        </div>
      </div>
    </div>
  );
}

export default Card;
