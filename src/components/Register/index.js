import React from 'react';
import './styles.css';
import { Link } from 'react-router-dom';

function Register(props) {

  function handleSubmit(e) {
    //e.preventDefault();
    props.onRegister(e.target);
  }

  return(
    <div className="register row">
      <span className="register-title">REGISTRO</span>
      <form className="col s12 m8" onSubmit={handleSubmit} action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <div className="row">
          <div className="input-field col s6">
            <input name="email" id="email" type="email" className="validate"/>
            <label htmlFor="email">Correo electrónico</label>
          </div>
        </div>
        <div className="row">
          <div className="input-field col s6">
            <input name="password" id="password" type="password" className="validate" />
            <label className="active" htmlFor="password">Contraseña</label>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s6">
            <div id="paypal-payment" >
              <input type="hidden" name="cmd" value="_s-xclick"/>
              <input type="hidden" name="hosted_button_id" value="FSFYJELP2A7ML"/>
              <input type="hidden" name="on0" value=""/>
              <input type="hidden" name="currency_code" value="MXN"/>

                <select name="os0" >
                  <option value="1 mes">1 mes : $5.00 MXN - mensual</option>
                </select> <br/>

              <input type="image" src="https://www.paypalobjects.com/es_XC/MX/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea."/>
              <img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1"/>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="input-field col s6">
          <Link to="/" className="pink accent-2 waves-effect waves-light btn cancel">Cancelar</Link>
          </div>
        </div>
      </form>
    </div>
  );
}

export default Register;
