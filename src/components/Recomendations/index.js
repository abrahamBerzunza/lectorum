import React from 'react';
import './styles.css';
import Card from './styles.css'
import RecomendationCard from '../RecomendationCard'

function Recomendations(props) {
  return(
    <div>
      <div class="card vertical content">

        <h4 class="recomendation-title">Te recomendamos:</h4>

        <div class="book-grid">

        {
          props.books.map(book => {
            return <RecomendationCard 
              cover={book.cover}
              title={book.title}
              id={book.id}/>
          })
        }
        
        </div>

      </div>
    </div>
  );
}

export default Recomendations;
