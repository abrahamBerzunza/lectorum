import React from 'react';
import { Link } from 'react-router-dom';

function Login(props) {

  function handleSubmit(e) {
    e.preventDefault();
    props.onAuth(e.target);
  }

  return(
    <div className="register row">
      <span className="register-title">INICIAR SESIÓN</span>
      <form className="col s12 m8" onSubmit={handleSubmit}>
        <div className="row">
          <div className="input-field col s6">
            <input name="email" id="email" type="email" className="validate"/>
            <label htmlFor="email">Correo electrónico</label>
          </div>
        </div>
        <div className="row">
          <div className="input-field col s6">
            <input name="password" id="password" type="password" className="validate" />
            <label className="active" htmlFor="password">Contraseña</label>
          </div>
        </div>
        <button className="register-button pink accent-2 waves-effect waves-light btn" 
            href="#" 
            type="submit">
          Iniciar Sesión
        </button>
        <Link to="/" className="pink accent-2 waves-effect waves-light btn">Cancelar</Link>
      </form>
    </div>
  );
}

export default Login;