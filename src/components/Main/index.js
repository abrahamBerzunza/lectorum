import React, { Component } from 'react';
import Navbar from '../Navbar';
import Card from '../Card';
import firebase from 'firebase';
import './styles.css';

class Main extends Component {
  constructor(props) {
    super(props)

    this.state = {
      books: []
    }
  }

  componentDidMount() {
    const booksDB = firebase.database().ref().child('books');

    booksDB.on('child_added', snapshot => {
      this.setState({
        books: this.state.books.concat(snapshot.val())
      });
    });
  }
  render() {
    return(
      <div>
        <Navbar />
        <div className="book-grid">
          {
            this.state.books.map(book => (
              <Card image={book.cover} name={book.name} key={book.id} id={book.id}/>
            ))
          }
        </div>
      </div>
    );
  }
}

export default Main;
