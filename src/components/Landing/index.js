import React from 'react';
import './styles.css';
import logo from './lectorum-md.png';
import { Link } from 'react-router-dom';

function Landing() {
  return(
    <div className="landing">
      <div className="landing-logo">
        <img src={logo} alt="lectorum-md"/>
        <h3 className="landing-logo-slogan">TU BIBLIOTECA EN LÍNEA</h3>
      </div>
      <h2 className="landing-phrase">
        Disfruta el mejor contenido a un precio accesible
      </h2>
      <div>
        <Link to="/login" className="landing-login pink accent-2 waves-effect waves-light btn">Iniciar Sesión</Link>
        <Link to="/register" className="landing-register pink accent-2 waves-effect waves-light btn">Regístrate</Link>
      </div>
    </div>
  );
}

export default Landing;