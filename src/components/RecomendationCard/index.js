import React from 'react';
import './styles.css';

function RecomendationCard(props) {
  return(
    <div>
    <div class="book-item">
      <img class="recomendation-book-image" src={props.cover}/>
      <div class="book-overlay">
        <div class="text">
          <p><strong class="book-title">{props.title}</strong></p>
          <a href={`/details?id=${props.id}`} class="pink accent-2 waves-effect waves-light btn">Más</a>
        </div>
      </div>
    </div>
    </div>
  );
}

export default RecomendationCard;
