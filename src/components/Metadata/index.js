import React from 'react';
import './styles.css';

function Metadata(props) {

  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  return(
    <div>
      {
        props.books
          .filter(book => { return getParameterByName('id') === book.id })
          .map(book => (
            <div className="col s12 m6">
              <div className="card horizontal content">
                <div className="card-image">
                  <img id="imagen" src={book.cover}/>
                </div>
                <div className="card-stacked">
                  <div className="card-content">
                    <p><h2>{book.title}</h2></p>
                    <p><strong>Autor: {book.author}</strong></p>
                    <p><strong>Editorial: {book.editorial}</strong></p>
                    <p><strong>Valoración: {book.score}</strong></p>
                    <p><strong>Descripción: {book.description}</strong></p>
                  </div>
                </div>

                <a className="register-button pink accent-2 waves-effect waves-light btn download-button"
                    href={book.downloadURL}
                    type="button">
                  Descargar
                </a>
              </div>
            </div>
          ))
      }
    </div>
  );
}

export default Metadata;
