import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import firebase from 'firebase';

// Initialize Firebase
var config = {
  apiKey: "AIzaSyDj9mpSyBqLBnvkKOWo7tMft8H04DYQLVI",
  authDomain: "nuevoproyecto-44df6.firebaseapp.com",
  databaseURL: "https://nuevoproyecto-44df6.firebaseio.com",
  projectId: "nuevoproyecto-44df6",
  storageBucket: "nuevoproyecto-44df6.appspot.com",
  messagingSenderId: "748002187863"
};
firebase.initializeApp(config);

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
