import React, { Component } from 'react';
import './App.css';
import Landing from './components/Landing';
import Register from './components/Register';
import Login from './components/Login';
import Details from './components/Details'
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import firebase from 'firebase';
import Main from './components/Main';

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      books: []
    }

    this.handleRegister = this.handleRegister.bind(this);
  }

  componentDidMount() {
    const booksDB = firebase.database().ref().child('books');

    booksDB.on('child_added', snapshot => {
      this.setState({
        books: this.state.books.concat(snapshot.val())
      });
    });
  }

  handleRegister(target) {
    let email = target.email.value
    let password = target.password.value

    firebase.auth().createUserWithEmailAndPassword(email, password)
      .catch(function(error) {
        console.log(error);
      });
  }

  handleAuth(target) {
    let email = target.email.value;
    let password = target.password.value;

    firebase.auth().signInWithEmailAndPassword(email, password).then(function(){
      window.location.pathname = "/main";
    }).catch(function(error) {
        console.log(error)
      });
  }

  render() {
    return (
      <div>
        <Router>
          <div>
            <Route exact path="/" component={Landing}/>
            <Route path="/register" render={ () => <Register onRegister={this.handleRegister}/>}/>
            <Route path="/login" render={ () => <Login onAuth={this.handleAuth}/>}/>
            <Route path="/main" component={Main}/>
            <Route path="/details" render={() => <Details books={this.state.books}/>}/>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
